#!/bin/bash
echo "Setting up permissions for openproject."
chmod -R 777 /tmp 
chmod -R 777 /var/db/openproject/
echo "Done!"