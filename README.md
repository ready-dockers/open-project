# OpenProject
OpenProject is a web-based project management system for location-independent team collaboration.

## config
database `MYSQL_ROOT_PASSWORD`, `MYSQL_DATABASE`, `DATABASE_URL` configure into docker-compose.yml file.
Additionlay `MYSQL_USER`, `MYSQL_PASSWORD` can be set to create new user.

## run 
OpenProject requires [docker](https://www.docker.com/) to run 
```sh
docker-compose up
```
Verfiry the deployment by navigating to you server address in your prefered browser.
```sh
http://localhost:8080
```


